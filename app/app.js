var Stardew = angular.module('Stardew', ['ui.router']);


console.log("app.js loaded");


Stardew.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/home");

    $stateProvider
        .state('home', {
          url: "/home",
          templateUrl: "app/views/home.html"
        })
        .state('villager', {
          url: "/villager/{selecteditemlower}",
          templateUrl: "app/views/villager.html"
        })
        .state('crop', {
          url: "/crop/{selecteditemlower}",
          templateUrl: "app/views/crop.html"
        })
        .state('cooking', {
          url: "/cooking/{selecteditemlower}",
          templateUrl: "app/views/cooking.html"
        })
        .state('event', {
          url: "/event/{selecteditemlower}",
          templateUrl: "app/views/event.html"
        })
        .state('location', {
          url: "/location/{selecteditemlower}",
          templateUrl: "app/views/location.html"
        })
        .state('quest', {
          url: "/quest/{selecteditemlower}",
          templateUrl: "app/views/quest.html"
        })
        .state('forage', {
          url: "/forage/{selecteditemlower}",
          templateUrl: "app/views/forage.html"
        })
        .state('furnishing', {
          url: "/furnishing/{selecteditemlower}",
          templateUrl: "app/views/furnishing.html"
        })
        .state('monster', {
          url: "/monster/{selecteditemlower}",
          templateUrl: "app/views/monster.html"
        })
        .state('ingredient', {
          url: "/ingredient/{selecteditemlower}",
          templateUrl: "app/views/ingredient.html"
        })
        .state('resource', {
          url: "/resource/{selecteditemlower}",
          templateUrl: "app/views/resource.html"
        })
        .state('tool', {
          url: "/tool/{selecteditemlower}",
          templateUrl: "app/views/tool.html"
        })
        .state('livestock', {
          url: "/livestock/{selecteditemlower}",
          templateUrl: "app/views/livestock.html"
        })
        .state('fish', {
          url: "/fish/{selecteditemlower}",
          templateUrl: "app/views/fish.html"
        })
        .state('crafting', {
          url: "/crafting/{selecteditemlower}",
          templateUrl: "app/views/crafting.html"
        })
        .state('ability', {
          url: "/ability/{selecteditemlower}",
          templateUrl: "app/views/ability.html"
        })
        .state('bundle', {
          url: "/bundle/{selecteditemlower}",
          templateUrl: "app/views/bundle.html"
        })
        .state('farm', {
          url: "/farm/{selecteditemlower}",
          templateUrl: "app/views/farm.html"
        })
        .state('villager-list', {
              url: "/villager",
              templateUrl: "app/views/villager-list.html"
            })
        .state('crop-list', {
          url: "/crop",
          templateUrl: "app/views/crop-list.html"
        })
        .state('cooking-list', {
          url: "/cooking",
          templateUrl: "app/views/cooking-list.html"
        })
        .state('location-list', {
          url: "/location",
          templateUrl: "app/views/location-list.html"
        })
        .state('forage-list', {
          url: "/forage",
          templateUrl: "app/views/forage-list.html"
        })
        .state('furnishing-list', {
          url: "/furnishing",
          templateUrl: "app/views/furnishing-list.html"
        })
        .state('ingredient-list', {
          url: "/ingredient",
          templateUrl: "app/views/ingredient-list.html"
        })
        .state('resource-list', {
          url: "/resource",
          templateUrl: "app/views/resource-list.html"
        })
        .state('tool-list', {
          url: "/tool",
          templateUrl: "app/views/tool-list.html"
        })
        .state('crafting-list', {
          url: "/crafting",
          templateUrl: "app/views/crafting-list.html"
        })
        .state('event-list', {
          url: "/event",
          templateUrl: "app/views/event-list.html"
        })
        .state('quest-list', {
          url: "/quest",
          templateUrl: "app/views/quest-list.html"
        })
        .state('livestock-list', {
          url: "/livestock",
          templateUrl: "app/views/livestock-list.html"
        })
        .state('fish-list', {
          url: "/fish",
          templateUrl: "app/views/fish-list.html"
        })
        .state('monster-list', {
          url: "/monster",
          templateUrl: "app/views/monster-list.html"
        })
        .state('ability-list', {
          url: "/ability",
          templateUrl: "app/views/ability-list.html"
        })
        .state('bundle-list', {
          url: "/bundle",
          templateUrl: "app/views/bundle-list.html"
        })
        .state('farm-list', {
          url: "/farm",
          templateUrl: "app/views/farm-list.html"
        });
  });

 Stardew.controller('StardewController', function($scope, $http, $state) {

     $(document).ready(function() {
            $('select').material_select();
            $(".dropdown-button").dropdown();
            $(".button-collapse").sideNav();
     });




     console.log(items);
     var str = JSON.stringify(items);
    //  console.log(str);

     $scope.items = items;

     //Setting up bloodhound
    var search = new Bloodhound({
    datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    local: items
      });

      search.initialize();

      $('.typeahead').typeahead({
          hint: true,
          highlight: true
      }, {
        displayKey: 'name',
        source: search.ttAdapter()
      }).on('typeahead:selected', function(event, item) {

            $state.go('home');
            $state.go(item.type);
            $scope.itemselectedname = item.name;
            $scope.itemselectednamelower = item.name.toLowerCase();
            $scope.itemselectedurl = item.url;
            $scope.itemselectedintro = item.intro;
            $scope.itemselectedbuy = item.buy;
            $scope.itemselectedsell = item.sell;
            $scope.itemselectedhours = item.hours;
            $scope.itemselectedcraft = item.craft;
            $scope.itemselectedreward = item.reward;
            $scope.itemselectedtrigger = item.trigger;
            $scope.itemselectedtime = item.time;
            $scope.itemselectedstep1 = item.step1;
            $scope.itemselectedlev1 = item.lev1;
            $scope.itemselectedfind = item.find;
            $scope.itemselectedfam = item.fam;

            console.log($scope.itemselectedname);
          // clearing the selection requires a typeahead method
          $(this).typeahead('val', '');
        });;

    $scope.itemSelected = function(item) {
        $scope.itemselectedname = item.name;
        $scope.itemselectednamelower = item.name.toLowerCase();
        $scope.itemselectedurl = item.url;
        $scope.itemselecteddob = item.dob;
        $scope.itemselectedintro = item.intro;
        $scope.itemselectedbuy = item.buy;
        $scope.itemselectedsell = item.sell;
        $scope.itemselectedhours = item.hours;
        $scope.itemselectedres = item.res;
        $scope.itemselectedcraft = item.craft;
        $scope.itemselectedreward = item.reward;
        $scope.itemselectedtrigger = item.trigger;
        $scope.itemselectedtime = item.time;
        $scope.itemselectedstep1 = item.step1;
        $scope.itemselectedlev1 = item.lev1;
        $scope.itemselectedfind = item.find;
        $scope.itemselectedfam = item.fam;
        $scope.itemselectedloved = item.loved;
        $scope.itemselectedliked = item.liked;
        $scope.itemselecteddisliked = item.disliked;
        $scope.itemselectedhated = item.hated;
        $scope.itemselectedres = item.res;

        $scope.itemselecteddaily = item.daily;
        $scope.itemselecteddailymon = item.daily["0"].mon;
        $scope.itemselecteddailymontime = item.daily["0"].mon["0"].time;

        console.log($scope.itemselectedname);
        console.log($scope.itemselecteddaily);
        console.log($scope.itemselecteddailymontime);
    };

    });
