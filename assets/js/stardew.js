angular.module( 'Stardew', [] )
    
    .controller('StardewController', function($scope, $http) {
    
    $scope.search = "";
    $scope.searchempty = true;
        
//   Search Indexing
    $scope.items = [
//   Crops
        {name: "Garlic",type: "crop",url: "/crop/garlic"},
        {name: "Parsnip",type: "crop",url: "/crop/parsnip"},
        {name: "Potato",type: "crop",url: "/crop/potato"},
        {name: "Pumpkin",type: "crop",url: "/crop/pumpkin"},
        {name: "Sunflower",type: "crop",url: "/crop/sunflower"},
        {name: "Bean",type: "crop",url: "/crop/bean"},
        {name: "Cauliflower",type: "crop",url: "/crop/cauliflower"},
        {name: "Corn",type: "crop",url: "/crop/corn"},
        {name: "Hops",type: "crop",url: "/crop/hops"},
        {name: "Grapes",type: "crop",url: "/crop/grapes"},
        {name: "Amaranth",type: "crop",url: "/crop/amaranth"},
//   Villagers
        {name: "Elliot",type: "villager",url: "/villager/elliot"},
        {name: "Sam",type: "villager",url: "/villager/sam"},
        {name: "Sebastian",type: "villager",url: "/villager/sebastian"},
        {name: "Harvey",type: "villager",url: "/villager/harvey"},
        {name: "Josh",type: "villager",url: "/villager/josh"},
        {name: "Maru",type: "villager",url: "/villager/maru"},
        {name: "Haley",type: "villager",url: "/villager/haley"},
        {name: "Penny",type: "villager",url: "/villager/penny"},
        {name: "Abigail",type: "villager",url: "/villager/abigail"},
        {name: "Leah",type: "villager",url: "/villager/leah"},
        {name: "Marnie",type: "villager",url: "/villager/marnie"},
        {name: "Pam",type: "villager",url: "/villager/pam"},
        {name: "Mr. Lewis",type: "villager",url: "/villager/mrlewis"},
        {name: "Clint",type: "villager",url: "/villager/clint"},
        {name: "Robin",type: "villager",url: "/villager/robin"},
        {name: "Evelyn",type: "villager",url: "/villager/evelyn"},
//    Animals
        {name: "Cow",type: "animal",url: "/animal/cow"},
        {name: "Pig",type: "animal",url: "/animal/pig"},
        {name: "Sheep",type: "animal",url: "/animal/sheep"},
        {name: "Horse",type: "animal",url: "/animal/horse"},
        {name: "Chicken",type: "animal",url: "/animal/chicken"},
        {name: "Rabbit",type: "animal",url: "/animal/rabbit"},
        {name: "Goat",type: "animal",url: "/animal/goat"},
//    Locations
        {name: "Stardrop Saloon",type: "location",url: "/location/stardropsaloon"},
        {name: "Farm",type: "location",url: "/location/farm"},
        {name: "Joja Mart",type: "location",url: "/location/jojamart"},
        {name: "Community Center",type: "location",url: "/location/communitycenter"},
//    Tools
        {name: "Axe",type: "tool",url: "/tool/axe"},
        {name: "Bomb",type: "tool",url: "/tool/bomb"},
        {name: "Fishing Rod",type: "tool",url: "/tool/fishingrod"},
        {name: "Hoe",type: "tool",url: "/tool/hoe"},
        {name: "Pickaxe",type: "tool",url: "/tool/pickaxe"},
        {name: "Sword",type: "tool",url: "/tool/sword"},
        {name: "Watering Can",type: "tool",url: "/tool/wateringcan"},
        {name: "Scythe",type: "tool",url: "/tool/scythe"}
    ];


    });