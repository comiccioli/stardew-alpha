 //   Search Indexing
 var items = [

     //   Crops
     {
         name: "Parsnip",
         type: "crop",
         intro: "A spring tuber closely related to the carrot. It has an earthy taste and is full of nutrients. Plant these in the spring. Takes 4 days to mature.",
         season: "Spring",
         effects: "+25 Energy, +10 Health",
         sell: "35g"
     },
     {
         name: "Potato",
         type: "crop",
         season: "Spring",
         grow: "6 Days",
         effects: "+25 Energy, +10 Health",
         sell: "80g"
     },
     {
         name: "Garlic",
         type: "crop",
         season: "Spring",
         grow: "4 Days",
         effects: "+25 Energy, +10 Health",
         effectss: "+28 Energy, 11 Health",
         sell: "60g",
         seed: "Garlic Seeds"
     },
     {
         name: "Kale",
         type: "crop",
         season: "Spring",
         grow: "6 Days",
         effects: "+50 Energy, +20 Health",
         effectss: "+70 Energy, 28 Health",
         sell: "110g",
         seed: "Kale Seeds"
     },
     {
         name: "Strawberry",
         type: "crop",
         season: "Spring",
         grow: "8 Days",
         effects: "+50 Energy, +20 Health",
         effectss: "+70 Energy, 28 Health",
         sell: "110g",
         seed: "Strawberry Seeds"
     },
     {
         name: "Green Bean",
         type: "crop",
         season: "Spring",
         grow: "10 Days",
         effects: "+25 Energy, +10 Health",
         effectss: "+35 Energy, 14 Health",
         sell: "40g",
         seed: "Green Bean Starter"
     },
     {
         name: "Cauliflower",
         type: "crop",
         season: "Spring",
         grow: "12 Days",
         effects: "+75 Energy, +30 Health",
         effectss: "+105 Energy, 42 Health",
         sell: "80g",
         seed: "Cauliflower Seeds"
     },
     {
         name: "Rhubarb",
         type: "crop",
         season: "Spring",
         grow: "13 Days",
         effects: "? Energy, ? Health",
         effectss: "? Energy, ? Health",
         sell: "220g",
         seed: "Rhubarb Seeds"
     },
     {
         name: "Wheat",
         type: "crop",
         season: "Summer",
         grow: "4 Days",
         sell: "25g",
         seed: "Wheat Seeds"
     },
     {
         name: "Hot Pepper",
         type: "crop",
         season: "Summer",
         grow: "5 Days",
         effects: "+13 Energy, +5 Health",
         effectss: "+18 Energy, +7 Health",
         sell: "110g",
         seed: "Hot Pepper Seeds"
     },
     {
         name: "Radish",
         type: "crop",
         season: "Summer",
         grow: "6 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "90g",
         seed: "Radish Seeds"
     },
     {
         name: "Poppy",
         type: "crop",
         season: "Summer",
         grow: "7 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "140g",
         seed: "Poppy Seeds"
     },
     {
         name: "Summer Spangle",
         type: "crop",
         season: "Summer",
         grow: "8 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "90g",
         seed: "Summer Spangle Seeds"
     },
     {
         name: "Red Cabbage",
         type: "crop",
         season: "Summer",
         grow: "9 Days",
         effects: "+75 Energy, +30 Health",
         effectss: "+105 Energy, +42 Health",
         sell: "260g",
         seed: "Red Cabbage Seeds"
     },
     {
         name: "Hops",
         type: "crop",
         season: "Summer",
         grow: "11 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "25g",
         seed: "Hops Starter"
     },
     {
         name: "Tomato",
         type: "crop",
         season: "Summer",
         grow: "11 Days",
         effects: "+20 Energy, +8 Health",
         effectss: "+28 Energy, +11 Health",
         sell: "90g",
         seed: "Tomato Seeds"
     },
     {
         name: "Melon",
         type: "crop",
         season: "Summer",
         grow: "12 Days",
         effects: "+113 Energy, +45 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "250g",
         seed: "Melon Seeds"
     },
     {
         name: "Blueberry",
         type: "crop",
         season: "Summer",
         grow: "13 Days",
         effects: "+25 Energy, +10 Health",
         effectss: "+35 Energy, +14 Health",
         sell: "80g",
         seed: "Blueberry Seeds"
     },
     {
         name: "Starfruit",
         type: "crop",
         season: "Summer",
         grow: "13 Days",
         effects: "+125 Energy, +50 Health",
         effectss: "+175 Energy, +70 Health",
         sell: "220g",
         seed: "Starfruit Seeds"
     },
     {
         name: "Corn",
         type: "crop",
         season: ["Summer", "Fall"],
         grow: "14 Days",
         regrow: "4 Days",
         effects: "+25 Energy, +10 Health",
         effectss: "+35 Energy, +14 Health",
         sell: "90g",
         seed: "Corn Seeds"
     },
     {
         name: "Bok Choy",
         type: "crop",
         season: "Fall",
         grow: "4 Days",
         effects: "+25 Energy, +10 Health",
         effectss: "+35 Energy, +14 Health",
         sell: "80g",
         seed: "Bok Choy Seeds"
     },
     {
         name: "Eggplant",
         type: "crop",
         season: "Fall",
         grow: "5 Days",
         effects: "+20 Energy, +8 Health",
         effectss: "+28 Energy, +11 Health",
         sell: "60g",
         seed: "Eggplant Seeds"
     },
     {
         name: "Beet",
         type: "crop",
         season: "Fall",
         grow: "6 Days",
         effects: "+30 Energy, +12 Health",
         effectss: "+42 Energy, +16 Health",
         sell: "100g",
         seed: "Beet Seeds"
     },
     {
         name: "Cranberry",
         type: "crop",
         season: "Fall",
         grow: "7 Days",
         regrow: "5 Days",
         effects: "+38 Energy, +15 Health",
         effectss: "+53 Energy, +21 Health",
         sell: "130g",
         seed: "Cranberry Seeds"
     },
     {
         name: "Amaranth",
         type: "crop",
         season: "Fall",
         grow: "7 Days",
         effects: "+50 Energy, +20 Health",
         effectss: "+70 Energy, +28 Health",
         sell: "150g",
         seed: "Amaranth Seeds"
     },
     {
         name: "Sunflower",
         type: "crop",
         season: "Fall",
         grow: "8 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "+63 Energy, +25 Health",
         sell: "80g",
         seed: "Sunflower Seeds"
     },
     {
         name: "Artichoke",
         type: "crop",
         season: "Fall",
         grow: "8 Days",
         effects: "+30 Energy, +12 Health",
         effectss: "+42 Energy, +16 Health",
         sell: "160g",
         seed: "Artichoke Seeds"
     },
     {
         name: "Yam",
         type: "crop",
         season: "Fall",
         grow: "10 Days",
         effects: "+45 Energy, +18 Health",
         effectss: "? Energy, ? Health",
         sell: "160g",
         seed: "Yam Seeds"
     },
     {
         name: "Grape",
         type: "crop",
         season: "Fall",
         grow: "10 Days",
         regrow: "3 Days",
         effects: "+38 Energy, +15 Health",
         effectss: "+53 Energy, +21 Health",
         sell: "80g",
         seed: "Grape Seeds"
     },
     {
         name: "Fairy Rose",
         type: "crop",
         season: "Fall",
         grow: "12 Days",
         effects: "+45 Energy, +18Health",
         effectss: "+63 Energy, +25 Health",
         sell: "290g",
         seed: "Fairy Rose Seeds"
     },
     {
         name: "Pumpkin",
         type: "crop",
         season: "Fall",
         grow: "13 Days",
         sell: "320g",
         seed: "Pumpkin Seeds"
     },
     {
         name: "Sweet Gem Berry",
         type: "crop",
         season: "Fall",
         grow: "24 Days",
         effects: "? Energy, ? Health",
         effectss: "? Energy, ? Health",
         sell: "3000g",
         sells: "3750g",
         sellss: "4500g",
         seed: "Rare Seed"
     },
     {
         name: "Ancient Fruit",
         type: "crop",
         season: ["Fall", "Spring", "Summer"],
         grow: "28 Days",
         regrow: "8 Days",
         effects: "? Energy, ? Health",
         effectss: "? Energy, ? Health",
         sell: "750g",
         seed: "Ancient Fruit Seed"
     },
     //   Villagers
     {
         name: "Elliott",
         type: "villager",
         url: "/villager/elliott",
         villager: "Bachelor ",
         g: "Male",
         intro: "Elliott lives alone in a cabin on the beach. He is a writer who dreams of one day writing a magnificent novel. He is a sentimental \"romantic\" with a tendency to go off onto flowery, poetic tangents. When he can afford it, he enjoys a strong beverage at the saloon. Could a humble farmer such as yourself be the inspiration Elliott is looking for? There's only one way to find out...",
         dob: "5th of Fall",
         res: "Elliot's Cabin, Pelican Town"
     },
     {
         name: "Sam",
         type: "villager",
         url: "/villager/sam",
         villager: "Bachelor ",
         g: "Male",
         intro: "Sam is an outgoing, friendly guy who is brimming with youthful energy. He plays guitar and drums, and wants to start a band with Sebastian as soon as he has enough songs together. However, he does have a habit of starting ambitious projects and not finishing them. Sam is a little stressed about the impending return of his father, who has been away for years due to his line of work.",
         dob: "17th of Summer",
         fam: [{
             name: "Jodi",
             relation: "Mother"
         }, {
             name: "Vincent",
             relation: "Brother"
         }, {
             name: "Kent",
             relation: "Father"
         }]
     },
     {
         name: "Sebastian",
         type: "villager",
         url: "/villager/sebastian",
         villager: "Bachelor ",
         g: "Male",
         intro: "Sebastian is a rebellious loner who lives in his parents' basement. He is Maru's older half-brother, and feels like his sister gets all the attention and adoration, while he is left to rot in the dark. He tends to get deeply absorbed in computer games, comic books, and sci-fi novels, and will sometimes spend great lengths of time pursuing these hobbies alone in his room. He can be a bit unfriendly to people he doesn't know. Could a charming new farmer cultivate the wasteland of his heart? Who knows?",
         dob: "10th of Winter",
         fam: [{
             name: "Robin",
             relation: "Mother"
         }, {
             name: "Maru",
             relation: "Maternal Half Sister"
         }, {
             name: "Demetrius",
             relation: "Stepfather"
         }]
     },
     {
         name: "Harvey",
         type: "villager",
         url: "/villager/harvey",
         villager: "Bachelor ",
         g: "Male",
         intro: "Harvey is the town doctor. He's a little old for a bachelor, but he has a kind heart and a respected position in the community. He lives in a small apartment above the medical clinic, but spends most of his time working. You can sense a sadness about him, as if there's something he's not telling you...",
         dob: "14th of Winter"
     },
     {
         name: "Alex",
         type: "villager",
         url: "/villager/alex",
         villager: "Bachelor ",
         g: "Male",
         intro: "Alex loves sports and hanging out at the beach. He is quite arrogant and brags to everyone that he is going to be a professional athlete. Is his cockiness just a facade to mask his crushing self-doubt? Is he using his sports dream to fill the void left by the disappearance of his parents? Or is he just a brazen youth trying to \"look cool\"?",
         dob: "13th of Summer",
         fam: [{
             name: "George",
             relation: "Grandfather"
         }, {
             name: "Evelyn",
             relation: "Grandmother"
         }]
     },
     {
         name: "Maru",
         type: "villager",
         url: "/villager/maru",
         villager: "Bachelorette",
         g: "Female",
         intro: "Growing up with a carpenter and a scientist for parents, Maru acquired a passion for creating gadgets at a young age. When she isn't in her room, fiddling with tools and machinery, she sometimes does odd jobs at the local clinic.  Friendly, outgoing, and ambitious, Maru would be quite a lucky match for a lowly newcomer such as yourself... Can you win her heart, or will she slip through your fingers and disappear from your life forever?",
         dob: "10th of Summer",
         res: "24 Mountain Road, Pelican Town",
         fam: [{
             name: "Robin",
             relation: "Stepmother"
         }, {
             name: "Sebastian",
             relation: "Paternal Half Brother"
         }, {
             name: "Demetrius",
             relation: "Father"
         }]
     },
     {
         name: "Haley",
         type: "villager",
         url: "/villager/haley",
         villager: "Bachelorette",
         g: "Female",
         intro: "Being wealthy and popular throughout high school has made Haley a little conceited and self-centered. She has a tendency to judge people for superficial reasons.  But is it too late for her to discover a deeper meaning to life? Is there a fun, open-minded young woman hidden within that candy-coated shell?",
         dob: "14th of Spring",
         res: "2 Willow Lane, Pelican Town",
         fam: [{
             name: "Emily",
             relation: "Sister"
         }],
         daily: [{
             mon: [{
                 time: "10:30am",
                 act: "Leaves her room to eat in the kitchen."
             }, {
                 time: "11:00am",
                 do: "Goes to the river south of Marnie's Ranch to take photographs."
             }, {
                 time: "4:30pm",
                 act: "Heads back home for dinner at 5:30pm"
             }, {
                 time: "10pm",
                 act: "Goes to her room to fall asleep at 10:30pm."
             }, ],
         }],
         loved: ["Pink Cake", "Fruit Salad", "Sunflower", "Coconut"],
         liked: ["Cookie", "Ice Cream", "Pizza", "Salad", "Daffodil", "Sweet Pea", "Mayonnaise", "Wild Honey", "Amethyst", "Aquamarine", "Frozen Tear", "Topaz", "Tulip"],
         disliked: ["Dandelion", "Quartz", "Joja Cola"],
         hated: ["Wild Horseradish", "Clay", "Stone"]
     },
     {
         name: "Penny",
         type: "villager",
         url: "/villager/penny",
         villager: "Bachelorette",
         g: "Female",
         intro: "Penny lives with her mom, Pam, in a little trailer by the river. While Pam is out carousing at the saloon, Penny quietly tends to her chores in the dim, stuffy room she is forced to call home. She is shy and modest, without any grand ambitions for life other than settling in and starting a family. She likes to cook (although her skills are questionable) and read books from the local library.",
         dob: "2nd of Fall",
         fam: [{
             name: "Pam",
             relation: "Mother"
         }]
     },
     {
         name: "Abigail",
         type: "villager",
         url: "/villager/abigail",
         villager: "Bachelorette",
         g: "Female",
         intro: "Abigail lives at the general store with her parents. She sometimes fights with her mom, who worries about Abigail's \"alternative lifestyle\". Her mom has the following to say: \"I wish Abby would dress more appropriately and stop dyeing her hair blue. She has such a wonderful natural hair color, just like her grandmother did. Oh, and I wish she'd find some wholesome interests instead of this occult nonsense she's into.\" You might find Abigail alone in the graveyard, or maybe out in a rainstorm looking for frogs.",
         dob: "13th of Fall",
         res: "Leah's Cottage, Pelican Town",
         fam: [{
             name: "Caroline",
             relation: "Mother"
         }, {
             name: "Pierre",
             relation: "Father"
         }]
     },
     {
         name: "Leah",
         type: "villager",
         url: "/villager/leah",
         villager: "Bachelorette",
         g: "Female",
         intro: "Leah lives alone in a small cabin just outside of town. She loves to spend time outside, foraging for a wild meal or simply enjoying the gifts of the season. She's a talented artist with a large portfolio of work... yet she's too nervous to display it to the public. Maybe you can give her a little confidence boost?",
         dob: "23rd of Winter",
         loved: ["Salad", "Vegetable Stew", "Poppyseed Muffin", "Stir Fry", "Wine"],
         liked: ["Dandelion", "Morel", "Pale Ale", "Bok Choy", "Common Mushroom", "Honey"],
         disliked: ["Clay", "Stone", "Seaweed", "Small Mouth Bass", "Cave Carrot"],
         hated: ["Bread", "Pizza", "Poppy", "Void Egg"]
     },
     {
         name: "Marnie",
         type: "villager",
         url: "/villager/marnie",
         g: "Female"
     },
     {
         name: "Pam",
         type: "villager",
         url: "/villager/pam",
         g: "Female",
         dob: "18th of Spring"
     },
     {
         name: "Mr. Lewis",
         type: "villager",
         url: "/villager/mrlewis",
         g: "Male",
         dob: "7th of Spring"
     },
     {
         name: "Clint",
         type: "villager",
         url: "/villager/clint",
         g: "Male"
     },
     {
         name: "Robin",
         type: "villager",
         url: "/villager/robin",
         g: "Female"
     },
     {
         name: "Evelyn",
         type: "villager",
         url: "/villager/evelyn",
         g: "Female"
     },
     {
         name: "Caroline",
         type: "villager",
         url: "/villager/caroline",
         g: "Female"
     },
     {
         name: "Demetrius",
         type: "villager",
         url: "/villager/demetrius",
         g: "Male"
     },
     {
         name: "Dwarf",
         type: "villager",
         url: "/villager/dwarf",
         g: "Male"
     },
     {
         name: "Emily",
         type: "villager",
         url: "/villager/emily",
         g: "Female",
         dob: "27th of Spring"
     },
     {
         name: "George",
         type: "villager",
         url: "/villager/george",
         g: "Male"
     },
     {
         name: "Governor",
         type: "villager",
         url: "/villager/governor",
         g: "Male"
     },
     {
         name: "Gunther",
         type: "villager",
         url: "/villager/gunther",
         g: "Male"
     },
     {
         name: "Jas",
         type: "villager",
         url: "/villager/jas",
         g: "Male"
     },
     {
         name: "Jodi",
         type: "villager",
         url: "/villager/jodi",
         g: "Female"
     },
     {
         name: "Kent",
         type: "villager",
         url: "/villager/kent",
         g: "Male"
     },
     {
         name: "Krobus",
         type: "villager",
         url: "/villager/krobus",
         g: "Male"
     },
     {
         name: "Linus",
         type: "villager",
         url: "/villager/linus",
         g: "Male"
     },
     {
         name: "Marlon",
         type: "villager",
         url: "/villager/marlon",
         g: "Male"
     },
     {
         name: "Morris",
         type: "villager",
         url: "/villager/morris",
         g: "Male",
         intro: "Customer Satisfaction Representative of Joja Corporation."
     },
     {
         name: "Mr. Qi",
         type: "villager",
         url: "/villager/mrqi",
         g: "Male"
     },
     {
         name: "Pierre",
         type: "villager",
         url: "/villager/pierre",
         g: "Male",
         dob: "26th of Spring"
     },
     {
         name: "Sandy",
         type: "villager",
         url: "/villager/sandy",
         g: "Female"
     },
     {
         name: "Shane",
         type: "villager",
         url: "/villager/shane",
         g: "Male",
         dob: "20th of Spring"
     },
     {
         name: "Vincent",
         type: "villager",
         url: "/villager/vincent",
         g: "Male",
         dob: "10th of Spring"
     },
     {
         name: "Willy",
         type: "villager",
         url: "/villager/willy",
         g: "Male"
     },
     {
         name: "Wizard",
         type: "villager",
         url: "/villager/wizard",
         g: "Male"
     },

     //    Livestock
     {
         name: "Duck",
         type: "livestock",
         farm: "Coop",
         url: "/livestock/goat"
     },
     {
         name: "White Cow",
         type: "livestock",
         farm: "Barn",
         url: "/livestock/whitecow"
     },
     {
         name: "Brown Cow",
         type: "livestock",
         farm: "Barn",
         url: "/livestock/browncow"
     },
     {
         name: "Pig",
         type: "livestock",
         farm: "Barn",
         url: "/livestock/pig"
     },
     {
         name: "Sheep",
         type: "livestock",
         farm: "Barn",
         url: "/livestock/sheep"
     },
     {
         name: "Horse",
         type: "livestock",
         farm: "Stable",
         url: "/livestock/horse"
     },
     {
         name: "White Chicken",
         type: "livestock",
         farm: "Coop",
         url: "/livestock/whitechicken"
     },
     {
         name: "Brown Chicken",
         type: "livestock",
         farm: "Coop",
         url: "/livestock/brownchicken"
     },
     {
         name: "Rabbit",
         type: "livestock",
         farm: "Coop",
         url: "/livestock/rabbit"
     },
     {
         name: "Goat",
         type: "livestock",
         farm: "Barn",
         url: "/livestock/goat"
     },

     //    Fish
     {
         name: "Smallmouth Bass",
         type: "fish",
         url: "/fish/smallmouthbass",
         intro: "A freshwater fish that is very sensitive to pollution.",
         effects: "+25 Energy, +10 Health",
         find: "River",
         season: "Spring"
     },
     {
         name: "Sunfish",
         type: "fish",
         url: "/fish/sunfish",
         intro: "A common river fish.",
         effects: "+13 Energy, +5 Health",
         season: "Spring",
         find: "Ocean"
     },
     {
         name: "Mussel",
         type: "fish",
         url: "/fish/mussel",
         intro: "A common bivalve that often lives in clusters.",
         find: "Beach",
         sell: "30g"
     },

     //    Locations
     {
         name: "The Stardrop Saloon",
         type: "location",
         url: "/location/thestardropsaloon"
     },
     {
         name: "Farm",
         type: "location",
         url: "/location/farm"
     },
     {
         name: "Town Square",
         type: "location",
         url: "/location/townsquare"
     },
     {
         name: "JojaMart",
         type: "location",
         url: "/location/jojamart"
     },
     {
         name: "Community Center",
         type: "location",
         url: "/location/communitycenter"
     },
     {
         name: "Bath House",
         type: "location",
         url: "/location/bathhouse"
     },
     {
         name: "Wizard's Tower",
         type: "location",
         url: "/location/wizardstower"
     },
     {
         name: "Mines",
         type: "location",
         url: "/location/mines"
     },
     {
         name: "Park",
         type: "location",
         url: "/location/park"
     },
     {
         name: "Adventurer's Guild",
         type: "location",
         url: "/location/adventurersguild"
     },
     {
         name: "Blacksmith",
         type: "location",
         url: "/location/blacksmith"
     },
     {
         name: "Carpenter's Shop",
         type: "location",
         url: "/location/carpentersshop",
         res: "Robin, Demetrius, Sebastian & Maru"
     },
     {
         name: "Pierre's General Store",
         type: "location",
         url: "/location/pierresgeneralstore",
         hours: "9:00am to 9:00pm",
         res: "Pierre, Caroline & Abigail"
     },
     {
         name: "Mayor's Manor",
         type: "location",
         url: "/location/mayorsmanor",
         hours: "8:30am to 10:00pm"
     },

     //    Tools
     {
         name: "Axe",
         type: "tool",
         url: "/tool/axe",
         intro: "Used to chop wood."
     },
     {
         name: "Bomb",
         type: "tool",
         url: "/tool/bomb"
     },
     {
         name: "Bamboo Pole",
         type: "tool",
         url: "/tool/bamboopole",
         intro: "Use in the water to catch fish."
     },
     {
         name: "Hoe",
         type: "tool",
         url: "/tool/hoe",
         intro: "Used to dig and till soil."
     },
     {
         name: "Pickaxe",
         type: "tool",
         url: "/tool/pickaxe",
         intro: "Used to break stones."
     },
     {
         name: "Sword",
         type: "tool",
         url: "/tool/sword"
     },
     {
         name: "Watering Can",
         type: "tool",
         url: "/tool/wateringcan",
         intro: "Used to water crops. It can be refilled at any water source."
     },
     {
         name: "Scythe",
         type: "tool",
         url: "/tool/scythe",
         intro: "Used to cut grass into hay."
     },
     {
         name: "Slingshot",
         type: "tool",
         url: "/tool/slingshot"
     },

     //     Events
     {
         name: "Dance of the Midnight Jellies",
         type: "event",
         url: "/event/danceofthemidnightjellies",
         intro: "On a warm summer night every year, the midnight jellies pass through Stardew Valley on their long migration home.",
         date: ""
     },
     {
         name: "Harvest Fair",
         type: "event",
         url: "/event/harvestfair",
         intro: "A celebration of the harvest season. Artisans, farmers, and craftspeople alike prepare exhibits to show off their hard work from throughout the year. A panel of judges awards medals to those with the most compelling displays."
     },
     {
         name: "Luau",
         type: "event",
         url: "/event/luau",
         intro: "The summer luau is one of Stardew Valley's most beloved events. The beach is decked out with tropical adornments and a huge buffet is provided for everyone to enjoy. The Regional Governor makes a special trip each year to see what the valley has to offer... something that Mayor Lewis takes very seriously. The highlight of the luau has to be the potluck soup... a huge, simmering cauldron full of ingredients provided by the community."
     },
     {
         name: "Flower Dance",
         type: "event",
         url: "/event/flowerdance",
         intro: "The townsfolk congregate in a secret meadow to feast and socialize. The young people pair up and participate in a time-honored tradition, the \"Flower Dance\". It's your chance to show a special someone that you care... just hope that you don’t get rejected!",
         date: "24th of Spring"
     },
     {
         name: "Egg Festival",
         type: "event",
         url: "/event/eggfestival",
         intro: "Everyone gathers in town for a celebration of new life after a long winter. The festival concludes with an egg hunt.",
         date: "13th of Spring"
     },
     {
         name: "Stardew Valley Fair",
         type: "event",
         url: "/event/stardewvalleyfair",
         intro: "Play carnival games! Set up your own grange display and try to win the judging competition! Have your fortune told! Win Star Tokens and trade them in for rare prizes!"
     },
     {
         name: "Haunted Maze",
         type: "event",
         url: "/event/hauntedmaze",
         intro: "The strange hermit who lives in the forest set up the maze alone. He wouldn't let anyone near it until nightfall. What kind of uncanny power is at work here?"
     },
     {
         name: "Harvest Fair",
         type: "event",
         url: "/event/harvestfair",
         intro: ""
     },

     //     Seeds
     {
         name: "Mixed Seeds",
         type: "seed",
         url: "/seed/mixedseeds",
         intro: "There's a little bit of everything here. Plant them and see what grows!"
     },
     {
         name: "Acorn",
         type: "seed",
         url: "/seed/acorn",
         intro: "Place this on your farm to plant an oak tree."
     },
     {
         name: "Parsnip Seeds",
         type: "seed",
         url: "/seed/parsnipseeds",
         intro: "Plant these in Spring. Takes 4 days to mature.",
         buy: "20g at Pierre's General Store.",
         sell: "10g at Pierre's General Store."
     },
     {
         name: "Bean Starter",
         type: "seed",
         url: "/seed/beanstarter",
         intro: "Plant these in Spring. Takes 10 days to mature, but keeps producing after that. Yields multiple beans per harvest. Grows on a trellis",
         buy: "60g at Pierre's General Store."
     },
     {
         name: "Cauliflower Seeds",
         type: "seed",
         url: "/seed/cauliflowerseeds",
         intro: "Plant these in the Spring. Takes 12 days to produce a large cauliflower.",
         buy: "80g at Pierre's General Store."
     },
     {
         name: "Potato Seeds",
         type: "seed",
         url: "/seed/potatoseeds",
         intro: "Plant these in the Spring. Takes 6 days to mature, and has a chance of yielding multiple potatoes at harvest.",
         buy: "50g at Pierre's General Store.",
         sell: "25g at Pierre's General Store."
     },
     {
         name: "Tulip Bulb",
         type: "seed",
         url: "/seed/tulipbulb",
         intro: "Plant in the Spring. Takes 6 days to produce a colorful flower. Assorted colors.",
         buy: "20g at Pierre's General Store."
     },
     {
         name: "Kale Seeds",
         type: "seed",
         url: "/seed/kaleseeds",
         intro: "Plant these in the Spring. Takes 6 days to mature. Harvest with scythe.",
         buy: "70g at Pierre's General Store."
     },
     {
         name: "Jazz Seeds",
         type: "seed",
         url: "/seed/jazzseeds",
         intro: "Plant these in the Spring. Takes 7 days to produce a blue puffball flower.",
         buy: "30g at Pierre's General Store."
     },
     {
         name: "Grass Starter",
         type: "seed",
         url: "/seed/grassstarter",
         intro: "Place this on your farm to start a new patch of grass.",
         buy: "100g at Pierre's General Store."
     },
     {
         name: "Cherry Sapling",
         type: "seed",
         url: "/seed/cherrysapling",
         intro: "Takes 28 days to produce a mature Cherry tree. Bears fruit in the *season*. Only grows if 8 surrounding 'tiles' are empty.",
         buy: "3400g at Pierre's General Store."
     },
     {
         name: "Apricot Sapling",
         type: "seed",
         url: "/seed/apricotsapling",
         intro: "Takes 28 days to produce a mature Apricot tree. Bears fruit in the Spring. Only grows if 8 surrounding 'tiles' are empty.",
         buy: "2000g at Pierre's General Store."
     },
     {
         name: "Orange Sapling",
         type: "seed",
         url: "/seed/orangesapling",
         intro: "Takes 28 days to produce a mature Orange tree. Bears fruit in the Summer. Only grows if 8 surrounding 'tiles' are empty.",
         buy: "4000g at Pierre's General Store."
     },
     {
         name: "Peach Sapling",
         type: "seed",
         url: "/seed/peachsapling",
         intro: "Takes 28 days to produce a mature Peach tree. Bears fruit in the . Only grows if 8 surrounding 'tiles' are empty.",
         buy: "6000g at Pierre's General Store."
     },
     {
         name: "Pomengranate Sapling",
         type: "seed",
         url: "/seed/pomengranatesapling",
         intro: "Takes 28 days to produce a mature Pomengranate tree. Bears fruit in the Fall. Only grows if 8 surrounding 'tiles' are empty.",
         buy: "6000g at Pierre's General Store."
     },
     {
         name: "Apple Sapling",
         type: "seed",
         url: "/seed/applesapling",
         intro: "Takes 28 days to produce a mature Apple tree. Bears fruit in the Fall. Only grows if 8 surrounding 'tiles' are empty.",
         buy: "4000g at Pierre's General Store."
     },
     {
         name: "Rare Seed",
         type: "seed",
         url: "/seed/rareseed",
         intro: "Sow in the fall. Takes all season to grow.",
         buy: "1000g at the Pig Caravan."
     },

     //      Misc Items
     {
         name: "Clam",
         type: "misc",
         url: "/misc/clam",
         intro: "Someone lived here once.",
         find: "Beach",
         sell: "50g"
     },


     //     Trash
     {
         name: "Joja Cola",
         type: "trash",
         url: "/trash/jojacola",
         intro: "The flagship product of Joja Corporation",
         effects: "+13 Energy, +5 Health",
         buy: "75g at Stardrop Saloon's Vending Machine."
     },

     //      Ingredients
     {
         name: "Sugar",
         type: "ingredient",
         url: "/ingredient/sugar",
         intro: "Adds sweetness to pastries and candies. Too much can be unhealthy.",
         buy: "100g at Pierre's General Store.",
         effects: "+25 Energy"
     },
     {
         name: "Wheat Flour",
         type: "ingredient",
         url: "/ingredient/wheatflour",
         intro: "A common cooking ingredient made from crushed wheat seeds.",
         buy: "100g at Pierre's General Store.",
         effects: "+13 Energy"
     },
     {
         name: "Rice",
         type: "ingredient",
         url: "/ingredient/rice",
         intro: "A basic grain often served under vegetables.",
         buy: "200g at Pierre's General Store.",
         effects: "+13 Energy, +5 Health"
     },
     {
         name: "Vinegar",
         type: "ingredient",
         url: "/ingredient/vinegar",
         intro: "An aged fermented liquid used in many cooking recipes.",
         effects: "+13 Energy, +5 Health",
         buy: "200g at Pierre's General Store."
     },
     {
         name: "Oil",
         type: "ingredient",
         url: "/ingredient/oil",
         intro: "All purpose cooking oil.",
         effects: "+13 Energy, +5 Health",
         buy: "200g at Pierre's General Store."
     },

     {
         name: "Wallpaper",
         type: "furnishing",
         url: "/furnishing/wallpaper",
         intro: "Decorate the walls of one room.",
         buy: "100g at Pierre's General Store."
     },
     {
         name: "Flooring",
         type: "furnishing",
         url: "/furnishing/flooring",
         intro: "Decorate the floor of one room.",
         buy: "100g at Pierre's General Store."
     },

     //      TV Channels
     {
         name: "Weather  Report",
         type: "channel",
         url: "/channel/weatherreport",
         intro: ""
     },
     {
         name: "Fortune Teller",
         type: "channel",
         url: "/channel/fortuneteller",
         intro: "Ah, yes... I can hear the spirits whipering something to me... 1. The spirits are in good humor today. I think you'll have a little extra luck."
     },
     {
         name: "Livin' Off The Land",
         type: "channel",
         url: "/channel/livinofftheland",
         intro: ""
     },

     //      Resources
     {
         name: "Fiber",
         type: "resource",
         url: "/resource/fiber",
         intro: "Raw material sourced from plants.",
         sell: "1g"
     },
     {
         name: "Wood",
         type: "resource",
         url: "/resource/wood",
         intro: "A sturdy yet flexible material with a variety of uses.",
         buy: "10g at the Blacksmith"
     },
     {
         name: "Stone",
         type: "resource",
         url: "/resource/stone",
         intro: "A common material with many uses in crafting and building",
         buy: "10g at the Blacksmith",
         find: "Farm, the Mines"
     },
     {
         name: "Clay",
         type: "resource",
         url: "/resource/clay",
         intro: "Put in a furnace to make a clay pot."
     },
     {
         name: "Copper Ore",
         type: "resource",
         url: "/resource/copperore",
         intro: ""
     },
     {
         name: "Quartz",
         type: "resource",
         url: "/resource/quartz",
         intro: ""
     },
     {
         name: "Coal",
         type: "resource",
         url: "/resource/coal",
         intro: "A combustible rock that is useful for crafting and smelting."
     },
     {
         name: "Slime",
         type: "resource",
         url: "/resource/slime",
         intro: "",
         drop: "Slime"
     },

     //      Foraging
     {
         name: "Daffodil",
         type: "forage",
         url: "/forage/daffodil",
         intro: "A traditional spring flower that makes a nice gift.",
         effects: "0 Energy"
     },
     {
         name: "Dandelion",
         type: "forage",
         url: "/forage/dandelion",
         intro: "Not the prettiest flower, but the leaves make a good salad.",
         effects: "+25 Energy"
     },
     {
         name: "Sap",
         type: "forage",
         url: "/forage/sap",
         intro: "A fluid obtained from trees.",
         effects: "-2 Energy",
         sell: "2g"
     },
     {
         name: "Pine Cone",
         type: "forage",
         url: "/forage/pinecone",
         intro: "Place this on your farm to plant a pine tree.",
         effects: ""
     },
     {
         name: "Tulip",
         type: "forage",
         url: "/forage/pinecone",
         intro: "",
         effects: "",
         buy: "300g/900g at the Pig Caravan"
     },
     {
         name: "Cave Carrot",
         type: "forage",
         url: "/forage/cavecarrot",
         intro: "A starchy snack found in caves. It helps miners work longer.",
         effects: "+30 Energy",
         buy: "400g/900g at the Pig Caravan"
     },
     {
         name: "Egg",
         type: "animalproduct",
         url: "/animalproduct/egg",
         intro: "A regular brown chicken egg.",
         effects: "+25 Energy",
         buy: "125g at the Pig Caravan"
     },
     {
         name: "Honey",
         type: "artisangoods",
         url: "/artisangoods/honey",
         intro: "It's a sweet syrup produced by bees.",
         buy: "1000g at the Pig Caravan"
     },
     {
         name: "Stonefish",
         type: "forage",
         url: "/forage/cavecarrot",
         intro: "",
         effects: "",
         buy: "900g at the Pig Caravan"
     },
     {
         name: "Goat Milk",
         type: "animalproduct",
         url: "/animalproduct/goatmilk",
         intro: "The milk of a goat.",
         effects: "+63 Energy",
         buy: "450g at the Pig Caravan"
     },

     //      Furnishings
     {
         name: "Basic Window",
         type: "furnishing",
         url: "/furnishing/basicwindow",
         intro: "Can be placed inside your house.",
         buy: "200g at the Blacksmith"
     },
     {
         name: "Small Window",
         type: "furnishing",
         url: "/furnishing/smallwindow",
         intro: "Can be placed inside your house.",
         buy: "300g at the Blacksmith"
     },
     {
         name: "Country Lamp",
         type: "furnishing",
         url: "/furnishing/countrylamp",
         intro: "Can be placed inside your house.",
         buy: "500g at the Blacksmith"
     },
     {
         name: "Yellow Armchair",
         type: "furnishing",
         url: "/furnishing/yellowarmchair",
         intro: "Can be placed inside your house.",
         buy: "1000g at the Blacksmith"
     },
     {
         name: "Birch End Table",
         type: "furnishing",
         url: "/furnishing/birchendtable",
         intro: "Can be placed inside your house.",
         buy: "500g at the Blacksmith"
     },
     {
         name: "Oak End Table",
         type: "furnishing",
         url: "/furnishing/oakendtable",
         intro: "Can be placed inside your house.",
         buy: "1250g at the Pig Caravan"
     },
     {
         name: "China Cabinet",
         type: "furnishing",
         url: "/furnishing/chinacabinet",
         intro: "Can be placed inside your house.",
         buy: "6000g at the Blacksmith"
     },
     {
         name: "Budget TV",
         type: "furnishing",
         url: "/furnishing/budgettv",
         intro: "Can be placed inside your house.",
         buy: "750g at the Blacksmith"
     },
     {
         name: "Wooden Brazier Recipe",
         type: "furnishing",
         url: "/furnishing/woodenbrazierrecipe",
         intro: "Can be placed inside your house.",
         buy: "250g at the Blacksmith"
     },
     {
         name: "Wood Lamp-post Recipe",
         type: "furnishing",
         url: "/furnishing/woodlamppostrecipe",
         intro: "Can be placed inside your house.",
         buy: "500g at the Blacksmith"
     },
     {
         name: "Iron Lamp-post Recipe",
         type: "furnishing",
         url: "/furnishing/ironlamppostrecipe",
         intro: "Can be placed inside your house.",
         buy: "1000g at the Blacksmith"
     },
     {
         name: "Straw Floor Recipe",
         type: "furnishing",
         url: "/furnishing/strawfloorrecipe",
         intro: "Can be placed inside your house.",
         buy: "5000g at the Blacksmith"
     },
     {
         name: "Crystal Path Recipe",
         type: "furnishing",
         url: "/furnishing/crystalpathrecipe",
         intro: "Can be placed inside your house.",
         buy: "5000g at the Blacksmith"
     },
     {
         name: "Chest",
         type: "furnishing",
         url: "/furnishing/chest",
         intro: "A place to store your items",
         craft: "50 Wood"
     },
     {
         name: "Scarecrow",
         type: "furnishing",
         url: "/furnishing/scarecrow",
         intro: "Prevents crows from attacking your crops. Has a limited range \(about 20 \"tiles\"\).",
         craft: "50 Wood, 1 Coal, 20 Fiber"
     },
     {
         name: "Wood Fence",
         type: "furnishing",
         url: "/furnishing/woodfence",
         intro: "Keeps grass and animals contained!",
         craft: "2 Wood"
     },
     {
         name: "Gate",
         type: "furnishing",
         url: "/furnishing/gate",
         intro: "Allows you to pass through a fence",
         craft: "10 Wood"
     },

     //      Crafting
     {
         name: "Standard Detector",
         type: "crafting",
         url: "/crafting/standarddetector",
         intro: "Reveals the location of treasures",
         buy: "700g at the Pig Caravan"
     },
     {
         name: "Torch",
         type: "crafting",
         url: "/crafting/torch",
         intro: "Provides a modest amount of light",
         craft: "1 Wood, 2 Sap"
     },
     {
         name: "Wood Path",
         type: "crafting",
         url: "/crafting/woodpath",
         intro: "Place on the ground to create paths or to spruce up your floors.",
         craft: "1 Wood"
     },
     {
         name: "Gravel Path",
         type: "crafting",
         url: "/crafting/gravelpath",
         intro: "Place on the ground to create paths or to spruce up your floors.",
         craft: "1 Stone"
     },
     {
         name: "Cobblestone Path",
         type: "crafting",
         url: "/crafting/cobblestonepath",
         intro: "Place on the ground to create paths or to spruce up your floors.",
         craft: "1 Stone"
     },
     {
         name: "Campfire",
         type: "crafting",
         url: "/crafting/campfire",
         intro: "Provides a moderate amount of light",
         craft: "10 Stone, 10 Wood, 10 Fiber"
     },

     //      Farm Additions
     {
         name: "Coop",
         type: "farm",
         url: "/farm/coop",
         intro: "Houses four coop-dwelling animals.",
         buy: "4000g, 300 Wood, and 100 Stone at the Blacksmith"
     },
     {
         name: "Barn",
         type: "farm",
         url: "/farm/barn",
         intro: "Houses four barn-dwelling animals.",
         buy: "6000g, 350 Wood, and 150 Stone at the Blacksmith"
     },

     //      Quests
     {
         name: "Introductions",
         type: "quest",
         url: "/quest/introductions",
         intro: "It would be a nice gesture to introduce yourself around town. Some people might be anxious to meet the new farmer.",
         step1: "Greet 28 people.",
         trigger: "Start the game!"
     },
     {
         name: "Getting Started",
         type: "quest",
         url: "/quest/gettingstarted",
         intro: "If you want to become a farmer, you have to start with the basics. Use a hoe to till the soil, then use the seed packet on the tilled soil to sow a crop. Water every day until the crop is ready for harvest.",
         reward: "100g",
         step1: "Cultivate and harvest a parsnip.",
         trigger: "Start the game!"
     },
     {
         name: "To The Beach",
         type: "quest",
         url: "/quest/tothebeach",
         intro: "Someone named Willy invited you to the beach south of town. He says he has something to give you.",
         reward: "100g",
         step1: "Visit the beach south of town before 5:00pm",
         trigger: "On the second day, the player recieves a letter from Willy."
     },
     {
         name: "Raising Animals",
         type: "quest",
         url: "/quest/raisinganimals",
         intro: "Robin, the local carpenter, lives north of town. In exchange for raw materials and money, she'll construct new buildings on your farm. You'll need her to build a coop or barn so that you can raise animals.",
         step1: "Build a Coop."
     },
     {
         name: "Advancement",
         type: "quest",
         url: "/quest/advancement",
         intro: "As you gain experience, you'll discover new crafting recipes to increase profit and make life easier. A scarecrow, for example, will prevent crows from snacking on your precious crops.",
         step1: "Reach Farming level 1 and craft a scarecrow.",
         reward: "100g"
     },
     {
         name: "Help Wanted",
         type: "quest",
         url: "/quest/helpwanted",
         intro: "I'm running low on Smallmouth Bass. If someone could bring me one, it would be much appreciated. -Robin",
         time: "2 Days",
         reward: "150g on delivery; Robin will be pleased.",
         step1: "Bring Robin a Smallmouth Bass",
         trigger: "Community Board Quest."
     },
     {
         name: "Delivery - Haley's Catfish",
         type: "quest",
         url: "/quest/deliveryhaleycatfish",
         intro: "I will <3 you forever if you bring me a Catfish! -Haley",
         time: "2 Days",
         reward: "600g on delivery; Haley will be pleased.",
         step1: "Bring Haley a Catfish",
         trigger: "Community Board Quest."
     },
     {
         name: "Exploring The Mine",
         type: "quest",
         url: "/quest/exploringthemine",
         intro: "There's an old mine shaft in the mountains north of town. There could be valuable minerals inside, but Marlon hinted that it might be dangerous",
         reward: "",
         step1: "Reach level 5 in the mines.",
         trigger: "Enter the Mine."
     },

     //      Levelling Abilities
     {
         name: "Farming",
         type: "ability",
         url: "/ability/farming",
         intro: "",
         lev1: "+1 Watering Can Proficiency, +1 Hoe proficiency, New crafting recipe: Scarecrow, New crafting recipe: Basic Fertilizer."
     },
     {
         name: "Foraging",
         type: "ability",
         url: "/ability/foraging",
         intro: "",
         lev1: "+1 Axe Proficiency, Trees sometimes drop seeds, New crafting recipe: Wild Seeds (Sp), New crafting recipe: Feld Snack."
     },
     {
         name: "Mining",
         type: "ability",
         url: "/ability/mining",
         intro: "",
         lev1: "+1 Pickaxe Proficiency, New crafting recipe: Cherry Bomb."
     },
     {
         name: "Foraging",
         type: "ability",
         url: "/ability/foraging",
         intro: "",
         lev1: ""
     },
     {
         name: "Fishing",
         type: "ability",
         url: "/ability/fishing",
         intro: "",
         lev1: ""
     },
     {
         name: "Combat",
         type: "ability",
         url: "/ability/combat",
         intro: "",
         lev1: ""
     },

     //      Community Center Bundles
     //bundles to Repair Bridge
     {
         name: "Summer Foraging Bundle",
         type: "bundle",
         url: "/bundle/summerforagingbundle",
         intro: "Part of the Crafting room.",
         craft: "Spice Berry, Grape and Sweet Pea"
     },
     {
         name: "Spring Foraging Bundle",
         type: "bundle",
         url: "/bundle/springforagingbundle",
         intro: "Part of the Crafting room.",
         craft: "Wild Horseradish, Daffodil, Leek and Dandelion"
     },
     {
         name: "Winter Foraging Bundle",
         type: "bundle",
         url: "/bundle/winterforagingbundle",
         intro: "Part of the Crafting room.",
         craft: ""
     },
     {
         name: "Fall Foraging Bundle",
         type: "bundle",
         url: "/bundle/fallforagingbundle",
         intro: "Part of the Crafting room.",
         craft: "Common Mushroom, Wild Plum, Hazelnut, Blackberry"
     },
     {
         name: "Exotic Foraging Bundle",
         type: "bundle",
         url: "/bundle/exoticforagingbundle",
         intro: "Part of the Crafting room.",
         craft: "Cave Carrot, Red Mushroom, Purple Mushroom, "
     },
     {
         name: "Construction Bundle",
         type: "bundle",
         url: "/bundle/constructionbundle",
         intro: "Part of the Crafting room.",
         craft: "99 Wood, 99 Wood, 99 Stone, 10 Hardwood"
     },

     //      Cooking
     {
         name: "Fried Egg",
         type: "cooking",
         intro: "Sunny-side up.",
         ingredients: ["Egg"],
         effects: "+50 Energy, +50 Health",
         sell: "35g"
     },
     {
         name: "Omelet",
         type: "cooking",
         intro: "It's super fluffy.",
         ingredients: ["Egg", "Milk"],
         effects: "+100 Energy, +40 Health",
         sell: "125g"
     },
     {
         name: "Salad",
         type: "cooking",
         intro: "A healthy garden salad.",
         ingredients: ["Leek", "Dandelion", "Vinegar"],
         effects: "+113 Energy, +45 Health",
         sell: "110g"
     },
     {
         name: "Cheese Cauliflower",
         type: "cooking",
         intro: "It smells great!",
         ingredients: ["Cauliflower", "Cheese"],
         effects: "+138 Energy, +55 Health",
         sell: "300g"
     },
 ];